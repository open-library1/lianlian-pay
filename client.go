/*
 * @Descripttion:
 * @version:
 * @Author: wanglei
 * @Date: 2023-04-26 12:06:24
 * @LastEditors: wanglei
 * @LastEditTime: 2023-04-28 15:02:40
 */
package pay_lianlian

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"sync"

	"gitlab.com/open-library1/lianlian-pay/xhttp"
	"gitlab.com/open-library1/lianlian-pay/xlog"
)

type Client struct {
	ApiVersion            string       //版本
	OidPartner            string       //测试商户号
	PrivateKey            []byte       //商户私钥
	PublicKey             []byte       //商户公钥
	LlPayPublicKey        []byte       //连连支付公钥
	SignType              string       //RSA
	IsProd                bool         //true 生产环境  false 测试环境
	Debug                 string       //on:开启Debug
	BaseDomain            string       //基础host
	BaseFileDomain        string       //上传文件host
	BasePwdDomain         string       //密码控件host
	BaseOpenAccountDomain string       //页面接入开户
	mu                    sync.RWMutex //
}

func NewClient(oidPartner string, llPayPublicKey, privateKey, publicKey []byte, isProd bool, debug string) (client *Client) {
	baseDomain := ""
	baseFileDomain := ""
	basePwdDomain := ""
	baseOpenAccountDomain := ""
	if isProd {
		baseDomain = baseUrlCh
		baseFileDomain = baseFileUrlCh
		basePwdDomain = basePwdUrlCh
		baseOpenAccountDomain = baseOpenAccountUrlCh
	} else {
		baseDomain = baseUrlChTest
		baseFileDomain = baseFileUrlChTest
		basePwdDomain = basePwdUrlChTest
		baseOpenAccountDomain = baseOpenAccountUrlChTest
	}
	return &Client{
		OidPartner:            oidPartner,
		LlPayPublicKey:        llPayPublicKey,
		PrivateKey:            privateKey,
		PublicKey:             publicKey,
		IsProd:                isProd,
		Debug:                 debug,
		BaseDomain:            baseDomain,
		BaseFileDomain:        baseFileDomain,
		BasePwdDomain:         basePwdDomain,
		BaseOpenAccountDomain: baseOpenAccountDomain,
		ApiVersion:            "1.0",
		SignType:              "RSA",
	}
}

// Post请求、正式
func (w *Client) doProdPost(ctx context.Context, bm BodyMap, url string, tlsConfig *tls.Config) (bs []byte, err error) {
	// 签名方式 (只支持RSA)
	var sign_type = w.SignType
	// 获取签名参数
	signStr := GetReleaseSign(w.PrivateKey, sign_type, bm)
	httpClient := xhttp.NewClient()
	httpClient.Header.Add("Content-Type", "application/json;charset=utf-8")
	httpClient.Header.Add("Signature-Type", w.SignType)
	httpClient.Header.Add("Signature-Data", signStr)
	if w.IsProd && tlsConfig != nil {
		httpClient.SetTLSConfig(tlsConfig)
	}

	// 请求接口
	req := BodyMapSotrJson(bm)
	if w.Debug == DebugOn {
		headerJson, _ := json.Marshal(httpClient.Header)
		xlog.Debugf("Lianlian_Request_url: %s", url)
		xlog.Debugf("Lianlian_Request: %s", req)
		xlog.Debugf("Lianlian_Request_Header: %s", headerJson)
	}
	res, bs, err := httpClient.Type(xhttp.TypeJSON).Post(url).SendString(req).EndBytes(ctx)
	if err != nil {
		return nil, err
	}
	if w.Debug == DebugOn {
		llHeaderJson, _ := json.Marshal(res.Header)
		xlog.Debugf("Lianlian_Response: %s%d %s%s", xlog.Red, res.StatusCode, xlog.Reset, string(bs))
		xlog.Debugf("Lianlian_Response_Header: %s", llHeaderJson)
	}

	// 获取签名数据
	sign, signType, err := GetHeaderSignInfo(res.Header)
	if err != nil {
		return nil, err
	}
	// 验证响应签名
	ok := VerifySign(w.LlPayPublicKey, signType, string(bs), sign)
	if !ok {
		return nil, errors.New("Lianlian_err: 响应签名错误")
	}

	if res.StatusCode != 200 {
		return nil, fmt.Errorf("HTTP Request Error, StatusCode = %d", res.StatusCode)
	}
	return bs, nil
}
