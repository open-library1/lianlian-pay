/*
 * @Descripttion:
 * @version:
 * @Author: wanglei
 * @Date: 2023-04-26 12:06:24
 * @LastEditors: wanglei
 * @LastEditTime: 2023-05-09 16:37:46
 */
package pay_lianlian

const (

	// 基础接口
	baseUrlCh     = "https://accpapi.lianlianpay.com/"
	baseUrlChTest = "https://accpapi-ste.lianlianpay-inc.com/"

	// 文件上传
	baseFileUrlCh     = "https://accpfile.lianlianpay.com/"
	baseFileUrlChTest = "https://accpfile-ste.lianlianpay-inc.com/"

	// 密码控件
	basePwdUrlCh     = "https://accpfile.lianlianpay.com/"
	basePwdUrlChTest = "https://accpgw-ste.lianlianpay-inc.com/"

	// 页面接入开户
	baseOpenAccountUrlCh     = "https://accpgw.lianlianpay.com/"
	baseOpenAccountUrlChTest = "https://accpgw-ste.lianlianpay-inc.com/"

	//业务接口
	//统一支付创单API
	payCreateBill = "v1/paycreatebill"
	//收款接口查询
	orderQuery = "orderquery.htm"

	//支付统一创单
	tradeCreate = "v1/txn/tradecreate"
	//网关类支付
	paymentGw = "v1/txn/payment-gw"

	// 绑定手机验证码申请
	regphoneVerifycodeApply = "v1/acctmgr/regphone-verifycode-apply"
	// 绑定手机验证码验证
	regphoneVerifycodeVerify = "v1/acctmgr/regphone-verifycode-verify"
	// 个人用户开户申请
	openacctApplyIndividual = "v1/acctmgr/openacct-apply-individual"
	// 个人用户开户验证
	openacctVerifyIndividual = "v1/acctmgr/openacct-verify-individual"
	// 随机密码因子获取
	getRandoml = "v1/acctmgr/get-random"
	// 申请密码控件Token
	applyPasswordElement = "v1/acctmgr/apply-password-element"
	// 用户信息查询
	queryUserinfo = "v1/acctmgr/query-userinfo"
	// 账户信息查询
	queryAcctinfo = "v1/acctmgr/query-acctinfo"
	// 资金流水列表查询
	queryAcctserial = "v1/acctmgr/query-acctserial"
	// 资金流水详情查询
	queryAcctserialdetail = "v1/acctmgr/query-acctserialdetail"
	// 个人用户新增绑卡申请
	individualBindcardApply = "v1/acctmgr/individual-bindcard-apply"
	// 个人用户新增绑卡验证
	individualBindcardVerify = "v1/acctmgr/individual-bindcard-verify"
	// 个人用户解绑银行卡
	unlinkedacctIndApply = "v1/acctmgr/unlinkedacct-ind-apply"
	// 绑卡信息查询
	queryLinkedacct = "v1/acctmgr/query-linkedacct"
	// 银行卡快捷支付
	paymentBankcard = "v1/txn/payment-bankcard"
	// 余额支付
	paymentBalance = "v1/txn/payment-balance"
	// 交易二次短信验证
	validationSms = "v1/txn/validation-sms"
	// 账户+收银台
	paycreate = "v1/cashier/paycreate"
	// 支付结果查询
	queryPayment = "v1/txn/query-payment"
	// 退款申请
	morePayeeRefund = "v1/txn/more-payee-refund"
	// 退款结果查询
	queryRefund = "v1/txn/query-refund"
	// 提现申请
	withdrawal = "v1/txn/withdrawal"
	// 提现结果查询
	queryWithdrawal = "v1/txn/query-withdrawal"
	// 内部代发申请
	transferMorepyee = "v1/txn/transfer-morepyee"
	// 文件上传
	fileUpload = "v1/documents/upload"
	// 用户开户页面接入
	openacctApply = "v1/acctmgr/openacct-apply"
	// 销户申请
	cancelApply = "v1/acctmgr/cancel-apply"

	// 签名方式
	SignType_MD5         = "MD5"
	SignType_HMAC_SHA256 = "HMAC-SHA256"
	SignType_MD5WithRSA  = "MD5WithRSA"
	SignType_RSA         = "RSA"

	RET_CODE_SUCCESS = "0000"
)

//统一下单返回
type UnifiedOrderResponse struct {
	RetCode     string `json:"ret_code"`     // 请求结果代码
	RetMsg      string `json:"ret_msg"`      // 请求结果描述
	OidPartner  string `json:"oid_partner"`  // ACCP系统分配给平台商户的唯一编号
	UserID      string `json:"user_id"`      // 商户用户唯一编号。用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。
	TotalAmount string `json:"total_amount"` // 订单总金额，单位为元，精确到小数点后两位
	TxnSeqno    string `json:"txn_seqno"`    // 商户系统唯一交易流水号。
	AccpTxno    string `json:"accp_txno"`    // ACCP系统交易单号
}

//订单收款结果查询
type QueryOrderResponse struct {
	DtOrder    string `json:"dt_order"`
	InfoOrder  string `json:"info_order"`
	MoneyOrder string `json:"money_order"`
	NoOrder    string `json:"no_order"`
	OidPartner string `json:"oid_partner"`
	OidPaybill string `json:"oid_paybill"`
	ResultPay  string `json:"result_pay"`
	SettleDate string `json:"settle_date"`
	RetCode    string `json:"ret_code"`
	RetMsg     string `json:"ret_msg"`
	PayType    string `json:"pay_type"`
	BankCode   string `json:"bank_code"`
	BankName   string `json:"bank_name"`
	CardNo     string `json:"card_no"`
	Sign       string `json:"sign"`
	SignType   string `json:"sign_type"`
}

//支付类回调通知
type PayNotifyResponse struct {
	OidPartner string `json:"oid_partner"`
	DtOrder    string `json:"dt_order"`
	NoOrder    string `json:"no_order"`
	OidPaybill string `json:"oid_paybill"`
	MoneyOrder string `json:"money_order"`
	ResultPay  string `json:"result_pay"`
	SettleDate string `json:"settle_date"`
	InfoOrder  string `json:"info_order"`
	PayType    string `json:"pay_type"`
	BankCode   string `json:"bank_code"`
	SignType   string `json:"sign_type"`
	Sign       string `json:"sign"`
}

//绑定手机验证码申请响应
type RegphoneVerifycodeApplyResponse struct {
	RetCode    string `json:"ret_code"`    // 请求结果代码
	RetMsg     string `json:"ret_msg"`     // 请求结果描述
	OidPartner string `json:"oid_partner"` // ACCP系统分配给平台商户的唯一编号
	UserID     string `json:"user_id"`     // 商户用户唯一编号。用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。
	RegPhone   string `json:"reg_phone"`   // 绑定手机号。用户开户绑定手机号。

}

//文件上传响应信息
type FileUploadResponse struct {
	RetCode    string `json:"ret_code"`    // 请求结果代码
	RetMsg     string `json:"ret_msg"`     // 请求结果描述
	OidPartner string `json:"oid_partner"` // ACCP系统分配给平台商户的唯一编号
	TxnSeqno   string `json:"txn_seqno"`   // 商户系统唯一交易流水号
	DocId      string `json:"doc_id"`      // 文件id
}

// 绑定手机验证码验证
type RegphoneVerifyCodeVerifyResponse struct {
	RetCode    string `json:"ret_code"`    // 请求结果代码
	RetMsg     string `json:"ret_msg"`     // 请求结果描述
	OidPartner string `json:"oid_partner"` // ACCP系统分配给平台商户的唯一编号
	UserID     string `json:"user_id"`     // 商户用户唯一编号。用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。
	RegPhone   string `json:"reg_phone"`   // 绑定手机号。用户开户绑定手机号。
}

// 个人用户开户申请
type OpenacctApplyIndividualResponse struct {
	RetCode    string `json:"ret_code"`    // 请求结果代码
	RetMsg     string `json:"ret_msg"`     // 请求结果描述
	OidPartner string `json:"oid_partner"` // ACCP系统分配给平台商户的唯一编号
	UserID     string `json:"user_id"`     // 商户用户唯一编号。用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。
	UserName   string `json:"user_name"`   // 用户名称。
	TxnSeqno   string `json:"txn_seqno"`   // 商户系统唯一交易流水号
	AccpTxno   string `json:"accp_txno"`   // ACCP系统交易单号
	Token      string `json:"token"`       // 授权令牌。有效期30分钟
}

// 个人用户开户验证
type OpenacctVerifyIndividualResponse struct {
	RetCode    string `json:"ret_code"`    // 请求结果代码
	RetMsg     string `json:"ret_msg"`     // 请求结果描述
	OidPartner string `json:"oid_partner"` // ACCP系统分配给平台商户的唯一编号
	UserID     string `json:"user_id"`     // 商户用户唯一编号。用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。
	TxnSeqno   string `json:"txn_seqno"`   // 商户系统唯一交易流水号
	AccpTxno   string `json:"accp_txno"`   // ACCP系统交易单号
	OidUserno  string `json:"oid_userno"`  // ACCP系统用户编号。用户注册成功后ACCP系统返回的用户编号
	UserStatus string `json:"user_status"` // 用户状态 ACTIVATE_PENDING :已登记或开户失败（原待激活）CHECK_PENDING :审核中（原待审核）REMITTANCE_VALID_PENDING :审核通过，待打款验证（企业用户使用，暂未要求）NORMAL :正常 CANCEL :销户 PAUSE :暂停 ACTIVATE_PENDING_NEW ：待激活
}

// 随机密码因子获取
type GetRandomlResponse struct {
	RetCode          string `json:"ret_code"`           // 请求结果代码
	RetMsg           string `json:"ret_msg"`            // 请求结果描述
	OidPartner       string `json:"oid_partner"`        // ACCP系统分配给平台商户的唯一编号
	UserID           string `json:"user_id"`            // 商户用户唯一编号。用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。
	License          string `json:"license"`            // flag_chnl为ANDROID、IOS、H5时返回
	MapArr           string `json:"map_arr"`            // 映射数组
	RandomKey        string `json:"random_key"`         // 随机因子key，有效期30分钟
	RandomValue      string `json:"random_value"`       // 随机因子值，有效期30分钟
	RsaPublicContent string `json:"rsa_public_content"` // 连连RSA公钥
	Sm2KeyHexString  string `json:"sm2_key_hexString"`  // 使用国密算法返回
}

// 用户开户回调请求信息
type OpenAccountNotifyRequest struct {
	AccountInfo  OpenAccountNotifyAccountInfo `json:"accountInfo"`
	OidPartner   string                       `json:"oid_partner"`   // 商户号，ACCP系统分配给平台商户的唯一编号
	UserID       string                       `json:"user_id"`       // ACCP系统分配给平台商户的唯一编号
	TxnSeqno     string                       `json:"txn_seqno"`     // 商户系统唯一交易流水号。由商户自定义
	AccpTxno     string                       `json:"accp_txno"`     // ACCP系统交易单号
	OidUserno    string                       `json:"oid_userno"`    // ACCP系统用户编号。用户注册成功后ACCP系统返回的用户编号
	UserStatus   string                       `json:"user_status"`   // 用户状态 ACTIVATE_PENDING :已登记或开户失败（原待激活）CHECK_PENDING :审核中（原待审核）REMITTANCE_VALID_PENDING :审核通过，待打款验证（企业用户使用，暂未要求）NORMAL :正常 CANCEL :销户 PAUSE :暂停 ACTIVATE_PENDING_NEW ：待激活
	LinkedAgrtno string                       `json:"linked_agrtno"` // 绑卡协议号
	Remark       string                       `json:"remark"`        // 备注。开户失败时，该字段内容是失败原因
	BankAccount  string                       `json:"bank_account"`  // 合作银行账号。ACCP合作银行账户
}

type OpenAccountNotifyAccountInfo struct {
	AccountLevel     string `json:"accountLevel"`     // 开户成功后返回的支付账户等级
	AccountNeedLevel string `json:"accountNeedLevel"` // 商户申请的等级，与account_level可能不一致
	AccountNeedType  string `json:"accountNeedType"`  // 商户申请的账户属性，与account_type可能不一致
	AccountType      string `json:"accountType"`      // 若该字段未上传，默认账户类型为账簿
}

// 申请密码控件Token
type ApplyPasswordElementResponse struct {
	RetCode              string `json:"ret_code"`               // 请求结果代码
	RetMsg               string `json:"ret_msg"`                // 请求结果描述
	OidPartner           string `json:"oid_partner"`            // ACCP系统分配给平台商户的唯一编号
	PasswordElementToken string `json:"password_element_token"` // 用于唤起密码控件的token

}

// 用户开户请求响应【页面接入】
type OpenacctApplyResponse struct {
	RetCode    string `json:"ret_code"`    // 请求结果代码
	RetMsg     string `json:"ret_msg"`     // 请求结果描述
	OidPartner string `json:"oid_partner"` // ACCP系统分配给平台商户的唯一编号
	GatewayURL string `json:"gateway_url"` // 用户开户网关地址，用户跳转至该地址完成开户过程。跳转方式：商户前端Get请求该地址。如果想要显示返回上一页按钮，可以在gateway_url后拼接header=Y，即可带上H5头部
	UserID     string `json:"user_id"`     // 商户用户唯一编号。用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户
	TxnSeqno   string `json:"txn_seqno"`   // 商户系统唯一交易流水号
	AccpTxno   string `json:"accp_txno"`   // ACCP系统交易单号
}

// 用户开户【页面接入】
type OpenacctApplyNotifyRequest struct {
	AccpTxno       string                            `json:"accp_txno"`      // ACCP系统交易单号
	BankAccount    string                            `json:"bank_account"`   // 合作银行账号。ACCP合作银行账户
	BasicInfo      OpenacctApplyNotifyBasicInfo      `json:"basicInfo"`      // 开户基本信息
	LinkedAcctInfo OpenacctApplyNotifyLinkedAcctInfo `json:"linkedAcctInfo"` // 开户绑卡信息
	LinkedAgrtno   string                            `json:"linked_agrtno"`  // 绑卡协议号
	OidPartner     string                            `json:"oid_partner"`    // 商户号，ACCP系统分配给平台商户的唯一编号
	OidUserno      string                            `json:"oid_userno"`     // ACCP系统用户编号。用户注册成功后ACCP系统返回的用户编号
	TxnSeqno       string                            `json:"txn_seqno"`      // 商户系统唯一交易流水号。由商户自定义
	UserID         string                            `json:"user_id"`        // 用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。由商户自定义
	UserStatus     string                            `json:"user_status"`    // 用户状态。ACTIVATE_PENDING :已登记或开户失败（原待激活） NORMAL :正常 NORMAL是审核通过，ACTIVATE_PENDING是审核未通过
}

type OpenacctApplyNotifyBasicInfo struct {
	Address    string `json:"address"`    // 个人用户住址或者企业地址
	IDExp      string `json:"id_exp"`     // 证件有效期。证件到期日，格式：yyyyMMdd 长期有效则设置为：99991231
	IDNo       string `json:"id_no"`      // 证件号码
	IDType     string `json:"id_type"`    // 证件类型。身份证：ID_CARD。 统一社会信用代码证：UNIFIED_SOCIAL_CREDIT_CODE。
	Occupation string `json:"occupation"` // 职业。职业列表:https://open.lianlianpay.com/docs/accp/accpstandard/openacct-apply.html，个人用户必填
	RegPhone   string `json:"reg_phone"`  // 绑定手机号。用户开户注册绑定手机号
	UserName   string `json:"user_name"`  // 用户名称。企业用户传企业名称，个人用户传姓名
}

type OpenacctApplyNotifyLinkedAcctInfo struct {
	LinkedAcctno     string `json:"linked_acctno"`     // 绑定银行账号。个人用户绑定的银行卡号，企业用户绑定的银行帐号
	LinkedBankcode   string `json:"linked_bankcode"`   // 银行编码。企业开户回调字段
	LinkedBrbankno   string `json:"linked_brbankno"`   // 对公账户开户行号。企业用户绑定账户开户支行行号。企业用户开户回调字段
	LinkedBrbankname string `json:"linked_brbankname"` // 对公账户开户行名。企业用户绑定账户开户支行名称。企业用户开户回调字段
	LinkedPhone      string `json:"linked_phone"`      // 银行预留手机号。个人用户开户回调参数
}

// 销户申请
type CancelApplyResponse struct {
	RetCode    string `json:"ret_code"`    // 请求结果代码
	RetMsg     string `json:"ret_msg"`     // 请求结果描述
	OidPartner string `json:"oid_partner"` // ACCP系统分配给平台商户的唯一编号
	UserID     string `json:"user_id"`     // 商户用户唯一编号。用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。
	TxnSeqno   string `json:"txn_seqno"`   // 商户系统唯一交易流水号
	AccpTxno   string `json:"accp_txno"`   // ACCP系统交易单号
}

// 注销结果异步通知
type CancelApplyNotifyRequest struct {
	OidPartner string `json:"oid_partner"`
	UserID     string `json:"user_id"`
	TxnSeqno   string `json:"txn_seqno"`
	AccpTxno   string `json:"accp_txno"`
	Result     string `json:"result"`
}

// 用户信息查询
type QueryUserinfoResponse struct {
	RetCode      string `json:"ret_code"`       // 请求结果代码
	RetMsg       string `json:"ret_msg"`        // 请求结果描述
	OidPartner   string `json:"oid_partner"`    // ACCP系统分配给平台商户的唯一编号
	BankAccount  string `json:"bank_account"`   // ACCP合作银行账户
	BankOpenFlag string `json:"bank_open_flag"` // 银行开户标识 0：未开户 1：已开户 2：开户中
	OidUserno    string `json:"oid_userno"`     // ACCP系统用户编号。用户注册成功后ACCP系统返回的用户编号。
	Remark       string `json:"remark"`         // 备注。审核不通过时，该字段内容是失败原因
	UserID       string `json:"user_id"`        // 商户用户唯一编号。用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。
	UserName     string `json:"user_name"`      // 用户名称。
	UserStatus   string `json:"user_status"`    // 用户状态 ACTIVATE_PENDING :已登记或开户失败（原待激活）CHECK_PENDING :审核中（原待审核）REMITTANCE_VALID_PENDING :审核通过，待打款验证（企业用户使用，暂未要求）NORMAL :正常 CANCEL :销户 PAUSE :暂停 ACTIVATE_PENDING_NEW ：待激活
}

// 账户信息查询
type QueryAcctinfoResponse struct {
	RetCode      string                      `json:"ret_code"`      // 请求结果代码
	RetMsg       string                      `json:"ret_msg"`       // 请求结果描述
	OidPartner   string                      `json:"oid_partner"`   // ACCP系统分配给平台商户的唯一编号
	AcctinfoList []QueryAcctinfoAcctinfoList `json:"acctinfo_list"` // 账户列表
	BankAccount  string                      `json:"bank_account"`  // ACCP合作银行账户
	OidUserno    string                      `json:"oid_userno"`    // ACCP系统用户编号。用户注册成功后ACCP系统返回的用户编号
	UserID       string                      `json:"user_id"`       // 用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。由商户自定义。查询商户账户信息时，则user_id不送
}
type QueryAcctinfoAcctinfoList struct {
	AcctState   string `json:"acct_state"`   // 账户状态。NORMAL :正常 CANCEL :注销 LOCK :锁定 LOSS :挂失 ACTIVATE_PENDING :待激活
	AcctType    string `json:"acct_type"`    // 账户类型
	AmtBalaval  string `json:"amt_balaval"`  // 可用余额。单位 元
	AmtBalcur   string `json:"amt_balcur"`   // 资金余额。单位 元
	AmtBalfrz   string `json:"amt_balfrz"`   // 冻结金额。单位 元
	AmtLastaval string `json:"amt_lastaval"` // 昨日可用余额。单位 元
	AmtLastbal  string `json:"amt_lastbal"`  // 昨日资金余额。单位 元
	AmtLastfrz  string `json:"amt_lastfrz"`  // 昨日冻结金额。单位 元
	OidAcctno   string `json:"oid_acctno"`   // 账户号
}

// 资金流水列表查询
type QueryAcctserialResponse struct {
	RetCode     string                       `json:"ret_code"`      // 请求结果代码
	RetMsg      string                       `json:"ret_msg"`       // 请求结果描述
	OidPartner  string                       `json:"oid_partner"`   // ACCP系统分配给平台商户的唯一编号
	AcctbalList []QueryAcctserialAcctbalList `json:"acctbal_list"`  // 资金流水列表
	PageNo      int                          `json:"page_no"`       // 请求页码。表示当前请求第几页，从1开始计数
	TotalInAmt  string                       `json:"total_in_amt"`  // 入账总金额，表示当前查询条件下的入账总金额，单位：元
	TotalNum    int                          `json:"total_num"`     // 结果集总数，表示当前查询条件下的结果集数据总数。
	TotalOutAmt string                       `json:"total_out_amt"` // 出账总金额，表示当前查询条件下的出账总金额，单位：元。
	TotalPage   int                          `json:"total_page"`    // 结果集总页数，total_page=(total_num/page_size) 向上取整。
	UserID      string                       `json:"user_id"`       // 用户在商户系统中的唯一编号。查询用户资金流水列表时必传。
}

type QueryAcctserialAcctbalList struct {
	AccpTxnno string `json:"accp_txnno"`        // 该笔资金流水对应的ACCP系统交易单号 accp_txno
	Amt       string `json:"amt"`               // 出入账金额。单位 元。
	AmtBal    string `json:"amt_bal"`           // 交易后余额。单位 元
	DateAcct  string `json:"date_acct"`         // 账务日期。交易账期，格式：yyyyMMdd
	FlagDc    string `json:"flag_dc"`           // 出入账标识。账户出入账标识， DEBIT：出账 CREDIT：入账 若不填写则表示不区分出入账
	JnoAcct   string `json:"jno_acct"`          // 资金流水号。ACCP账务系统资金流水唯一标识。
	OidAcctno string `json:"oid_acctno"`        // 账户号
	TxnTime   string `json:"txn_time"`          // 商户系统交易时间。格式：yyyyMMddHHmmss
	TxnType   string `json:"txn_type"`          // 交易类型。用户充值：USER_TOPUP 商户充值：MCH_TOPUP 普通消费：GENERAL_CONSUME 担保消费：SECURED_CONSUME 担保确认：SECURED_CONFIRM 内部代发：INNER_FUND_EXCHANGE 定向内部代发：INNER_DIRECT_EXCHANGE 外部代发：OUTER_FUND_EXCHANGE 账户提现：ACCT_CASH_OUT 手续费收取：SERVICE_FEE 手续费应收应付核销：CAPITAL_CANCEL 垫资调账：ADVANCE_PAY
	Memo      string `json:"memo,omitempty"`    // 资金流水备注
	JnoCli    string `json:"jno_cli,omitempty"` // 商户订单号。资金流水对应的商户交易单号，一条商户订单号可能对应多条资金流水。
}

// 资金流水详情查询
type QueryAcctserialdetailResponse struct {
	RetCode       string `json:"ret_code"`        // 请求结果代码
	RetMsg        string `json:"ret_msg"`         // 请求结果描述
	OidPartner    string `json:"oid_partner"`     // ACCP系统分配给平台商户的唯一编号
	AccpTxnno     string `json:"accp_txnno"`      // 该笔资金流水对应的ACCP系统交易单号：accp_txno
	Amt           string `json:"amt"`             // 出入账金额。单位 元。
	AmtBal        string `json:"amt_bal"`         // 交易后余额。单位 元
	DateAcct      string `json:"date_acct"`       // 账务日期。交易账期，格式：yyyyMMdd
	FlagDc        string `json:"flag_dc"`         // 出入账标识。账户出入账标识， DEBIT：出账 CREDIT：入账 若不填写则表示不区分出入账
	JnoAcct       string `json:"jno_acct"`        // 资金流水号。ACCP账务系统资金流水唯一标识。
	OidAcctno     string `json:"oid_acctno"`      // 账户号
	OtherAcct     string `json:"other_acct"`      // 对手账号。ACCP账户系统返回内部账户账号。快捷支付和提现返回卡号。微信支付宝不返回。
	OtherAcctName string `json:"other_acct_name"` // 对手户名。ACCP账户系统返回内部账户户名。快捷支付、提现和微信返回户名。支付宝不返回。
	TxnSeqno      string `json:"txn_seqno"`       // 商户系统唯一交易流水号。
	TxnTime       string `json:"txn_time"`        // 商户系统交易时间。格式：yyyyMMddHHmmss
	TxnType       string `json:"txn_type"`        // 交易类型。用户充值：USER_TOPUP 商户充值：MCH_TOPUP 普通消费：GENERAL_CONSUME 担保消费：SECURED_CONSUME 担保确认：SECURED_CONFIRM 内部代发：INNER_FUND_EXCHANGE 定向内部代发：INNER_DIRECT_EXCHANGE 外部代发：OUTER_FUND_EXCHANGE 账户提现：ACCT_CASH_OUT 手续费收取：SERVICE_FEE 手续费应收应付核销：CAPITAL_CANCEL 垫资调账：ADVANCE_PAY
	JnoCli        string `json:"jno_cli"`         // 商户订单号。资金流水对应的商户交易单号，一条商户订单号可能对应多条资金流水。
}

// 个人用户新增绑卡申请
type IndividualBindcardApplyResponse struct {
	RetCode    string `json:"ret_code"`    // 请求结果代码
	RetMsg     string `json:"ret_msg"`     // 请求结果描述
	OidPartner string `json:"oid_partner"` // ACCP系统分配给平台商户的唯一编号
	Token      string `json:"token"`       // 授权令牌。有效期30分钟
	UserID     string `json:"user_id"`     // 商户用户唯一编号。用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户
	TxnSeqno   string `json:"txn_seqno"`   // 商户系统唯一交易流水号
	AccpTxno   string `json:"accp_txno"`   // ACCP系统交易单号
}

// 个人用户新增绑卡验证
type IndividualBindcardVerifyResponse struct {
	RetCode      string `json:"ret_code"`      // 请求结果代码
	RetMsg       string `json:"ret_msg"`       // 请求结果描述
	OidPartner   string `json:"oid_partner"`   // ACCP系统分配给平台商户的唯一编号
	LinkedAgrtno string `json:"linked_agrtno"` // 绑卡协议号
	UserID       string `json:"user_id"`       // 商户用户唯一编号。用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户
	TxnSeqno     string `json:"txn_seqno"`     // 商户系统唯一交易流水号
	AccpTxno     string `json:"accp_txno"`     // ACCP系统交易单号
}

// 个人新增绑卡异步通知
type IndividualBindcardApplyNotifyRequest struct {
	OidPartner   string `json:"oid_partner"`   // ACCP系统分配给平台商户的唯一编号
	UserID       string `json:"user_id"`       // 用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。由商户自定义
	TxnSeqno     string `json:"txn_seqno"`     // 商户系统唯一交易流水号。由商户自定义
	AccpTxno     string `json:"accp_txno"`     // ACCP系统交易单号
	Action       string `json:"action"`        // 操作类型。LINKEDACCT_IND：个人用户新增绑卡
	Result       string `json:"result"`        // 操作结果。SUCCESS
	LinkedAgrtno string `json:"linked_agrtno"` // 绑卡协议号
}

// 个人用户解绑银行卡
type UnlinkedacctIndApplyResponse struct {
	RetCode    string `json:"ret_code"`    // 请求结果代码
	RetMsg     string `json:"ret_msg"`     // 请求结果描述
	OidPartner string `json:"oid_partner"` // ACCP系统分配给平台商户的唯一编号
	UserID     string `json:"user_id"`     // 用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。由商户自定义
	TxnSeqno   string `json:"txn_seqno"`   // 商户系统唯一交易流水号。由商户自定义
	AccpTxno   string `json:"accp_txno"`   // ACCP系统交易单号
}

// 银行卡解绑结果异步通知
type UnlinkedacctIndApplyNotifyRequest struct {
	OidPartner string `json:"oid_partner"` // ACCP系统分配给平台商户的唯一编号
	UserID     string `json:"user_id"`     // 用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。由商户自定义
	TxnSeqno   string `json:"txn_seqno"`   // 商户系统唯一交易流水号。由商户自定义
	AccpTxno   string `json:"accp_txno"`   // ACCP系统交易单号
	Action     string `json:"action"`      // 操作类型。UNLINKEDACCT_IND：个人解绑银行卡。
	Result     string `json:"result"`      // 操作结果。SUCCESS。
}

// 绑卡信息查询
type QueryLinkedacctResponse struct {
	RetCode        string                          `json:"ret_code"`        // 请求结果代码
	RetMsg         string                          `json:"ret_msg"`         // 请求结果描述
	OidPartner     string                          `json:"oid_partner"`     // ACCP系统分配给平台商户的唯一编号
	UserID         string                          `json:"user_id"`         // 用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。由商户自定义
	LinkedAcctlist []QueryLinkedacctLinkedAcctlist `json:"linked_acctlist"` // 银行卡列表
}
type QueryLinkedacctLinkedAcctlist struct {
	LinkedAcctno     string `json:"linked_acctno"`     // 个人用户绑定的银行卡号，企业用户绑定的银行帐号
	LinkedAgrtno     string `json:"linked_agrtno"`     // 绑卡协议号
	LinkedBankcode   string `json:"linked_bankcode"`   // 银行编码
	LinkedBrbankname string `json:"linked_brbankname"` // 对公账户开户行名。企业用户绑定账户开户支行名称
	LinkedPhone      string `json:"linked_phone"`      // 银行预留手机号
}

// 绑卡信息查询
type PaymentBankcardResponse struct {
	RetCode        string `json:"ret_code"`        // 请求结果代码
	RetMsg         string `json:"ret_msg"`         // 请求结果描述
	OidPartner     string `json:"oid_partner"`     // ACCP系统分配给平台商户的唯一编号
	UserID         string `json:"user_id"`         // 用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。由商户自定义
	TxnSeqno       string `json:"txn_seqno"`       // 商户系统唯一交易流水号。由商户自定义
	TotalAmount    string `json:"total_amount"`    // 订单总金额，单位为元，精确到小数点后两位
	AccountingDate string `json:"accounting_date"` // 账务日期。ACCP系统交易账务日期，交易成功时返回
	AccpTxno       string `json:"accp_txno"`       // ACCP系统交易单号
	Token          string `json:"token"`           // 支付授权令牌，有效期30分钟。当交易需要二次验证的时候，需要通过token调用调用交易二次短信验证接口
	FinishTime     string `json:"finish_time"`     // 支付完成时间,格式为yyyyMMddHHmmss
}

// 余额支付
type PaymentBalanceResponse struct {
	RetCode     string `json:"ret_code"`     // 请求结果代码
	RetMsg      string `json:"ret_msg"`      // 请求结果描述
	OidPartner  string `json:"oid_partner"`  // ACCP系统分配给平台商户的唯一编号
	UserID      string `json:"user_id"`      // 用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。由商户自定义
	TotalAmount string `json:"total_amount"` // 订单总金额，单位为元，精确到小数点后两位
	TxnSeqno    string `json:"txn_seqno"`    // 商户系统唯一交易流水号。由商户自定义
	AccpTxno    string `json:"accp_txno"`    // ACCP系统交易单号
	Token       string `json:"token"`        // 支付授权令牌，有效期30分钟。当交易需要二次验证的时候，需要通过token调用调用交易二次短信验证接口
}

// 交易二次短信验证
type ValidationSmsResponse struct {
	RetCode        string `json:"ret_code"`        // 请求结果代码
	RetMsg         string `json:"ret_msg"`         // 请求结果描述
	OidPartner     string `json:"oid_partner"`     // ACCP系统分配给平台商户的唯一编号
	UserID         string `json:"user_id"`         // 用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。由商户自定义
	TxnSeqno       string `json:"txn_seqno"`       // 商户系统唯一交易流水号。由商户自定义
	TotalAmount    string `json:"total_amount"`    // 订单总金额，单位为元，精确到小数点后两位
	AccountingDate string `json:"accounting_date"` // 账务日期。ACCP系统交易账务日期，交易成功时返回
	FinishTime     string `json:"finish_time"`     // 支付完成时间,格式为yyyyMMddHHmmss
	AccpTxno       string `json:"accp_txno"`       // ACCP系统交易单号
}

// 账户+收银台
type PaycreateResponse struct {
	RetCode     string `json:"ret_code"`     // 请求结果代码
	RetMsg      string `json:"ret_msg"`      // 请求结果描述
	OidPartner  string `json:"oid_partner"`  // ACCP系统分配给平台商户的唯一编号
	UserID      string `json:"user_id"`      // 用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。由商户自定义
	TotalAmount string `json:"total_amount"` // 订单总金额，单位为元，精确到小数点后两位
	TxnSeqno    string `json:"txn_seqno"`    // 商户系统唯一交易流水号。由商户自定义
	AccpTxno    string `json:"accp_txno"`    // ACCP系统交易单号
	GatewayURL  string `json:"gateway_url"`  // ACCP系统收银台首页地址。跳转方式：商户前端GET或location.href请求跳转。如果想隐藏收银台中的收款方信息，可以在返回的gateway_url后自行拼接noPayee=Y，然后再进行跳转。示例：https://accpgw.lianlianpay.com/mpay-cashier/?token=31a005f2e7dcb5bf3345c2a1924dadaf&noPayee=Y
}

// 支付结果异步通知
type PaymentNotifyRequest struct {
	OidPartner     string                   `json:"oid_partner"`     // ACCP系统分配给平台商户的唯一编号
	AccountingDate string                   `json:"accounting_date"` // 账务日期，ACCP系统交易账务日期，交易成功时返回，格式为yyyyMMdd
	FinishTime     string                   `json:"finish_time"`     // 支付完成时间。格式：yyyyMMddHHmmss。
	AccpTxno       string                   `json:"accp_txno"`       // ACCP系统交易单号
	TxnStatus      string                   `json:"txn_status"`      // 支付交易状态。TRADE_SUCCESS:交易成功 支付交易最终状态以此为准，商户按此进行后续业务逻辑处理
	Bankcode       string                   `json:"bankcode"`        // 银行编码
	OrderInfo      PaymentNotifyOrderInfo   `json:"orderInfo"`       // 商户订单信息
	PayerInfo      []PaymentNotifyPayerInfo `json:"payerInfo"`       // 付款方信息（组合支付场景返回付款方信息数组）
	PayeeInfo      []PaymentNotifyPayeeInfo `json:"payeeInfo"`       // 收款方信息（交易分账场景返回收款方信息数组）
}

type PaymentNotifyOrderInfo struct {
	TxnSeqno    string `json:"txn_seqno"`    // 商户系统唯一交易流水号。由商户自定义
	TxnTime     string `json:"txn_time"`     // 商户系统交易时间。格式：yyyyMMddHHmmss
	TotalAmount string `json:"total_amount"` // 订单总金额，单位为元，精确到小数点后两位
	OrderInfo   string `json:"order_info"`   // 订单信息，原样返回创单时传的订单信息
}

type PaymentNotifyPayerInfo struct {
	PayerType string `json:"payer_type"` // 付款方类型。用户：USER 平台商户：MERCHANT
	PayerID   string `json:"payer_id"`   // 付款方标识。付款方为用户时设置user_id 付款方为商户时设置平台商户号
	Method    string `json:"method"`     // 付款方式 https://open.lianlianpay.com/docs/accp/accpstandard/accp-async-notification-payment.html
	Amount    string `json:"amount"`     // 付款金额。付款方式对应的金额，单位为元，精确到小数点后两位
	Acctname  string `json:"acctname"`   // 付款银行户名，付款方式为银行卡支付和企业网银B2B支付方式时返回
	Acctno    string `json:"acctno"`     // 付款银行账号，付款方式为银行卡支付和企业网银B2B支付方式时返回，脱敏返回：卡号前六后四
}

type PaymentNotifyPayeeInfo struct {
	PayeeType string `json:"payee_type"` // 收款方类型。用户：USER 平台商户：MERCHANT
	PayeeID   string `json:"payee_id"`   // 收款方标识，收款方为用户时，为用户user_id，收款方为平台商户时，取平台商户号
	Amount    string `json:"amount"`     // 收款金额。单位为元，精确到小数点后两位
}

// 支付结果查询
type QueryPaymentResponse struct {
	RetCode        string                  `json:"ret_code"`        // 请求结果代码
	RetMsg         string                  `json:"ret_msg"`         // 请求结果描述
	OidPartner     string                  `json:"oid_partner"`     // ACCP系统分配给平台商户的唯一编号
	AccountingDate string                  `json:"accounting_date"` // 账务日期。ACCP系统交易账务日期，交易成功时返回
	FinishTime     string                  `json:"finish_time"`     // 支付完成时间,格式为yyyyMMddHHmmss
	AccpTxno       string                  `json:"accp_txno"`       // ACCP系统交易单号
	TxnStatus      string                  `json:"txn_status"`      // 支付交易状态。TRADE_SUCCESS:交易成功 支付交易最终状态以此为准，商户按此进行后续业务逻辑处理
	Bankcode       string                  `json:"bankcode"`        // 银行编号
	OrderInfo      QueryPaymentOrderInfo   `json:"orderInfo"`       // 商户订单信息
	PayerInfo      []QueryPaymentPayerInfo `json:"payerInfo"`       // 付款方信息（组合支付场景返回付款方信息数组）
	PayeeInfo      []QueryPaymentPayeeInfo `json:"payeeInfo"`       // 收款方信息（交易分账场景返回收款方信息数组）
}

type QueryPaymentOrderInfo struct {
	TxnSeqno    string `json:"txn_seqno"`    // 商户系统唯一交易流水号
	TxnTime     string `json:"txn_time"`     // 商户系统交易时间。格式：yyyyMMddHHmmss
	TotalAmount string `json:"total_amount"` // 订单总金额，单位为元，精确到小数点后两位
	OrderInfo   string `json:"order_info"`   // 用于订单说明，透传返回
}

type QueryPaymentPayerInfo struct {
	PayerType string `json:"payer_type"` // 付款方类型。用户：USER 平台商户：MERCHANT
	PayerID   string `json:"payer_id"`   // 付款方标识。付款方为用户时设置user_id 付款方为商户时设置平台商户号
	Method    string `json:"method"`     // 付款方式 https://open.lianlianpay.com/docs/accp/accpstandard/accp-async-notification-payment.html
	Amount    string `json:"amount"`     // 付款金额。付款方式对应的金额，单位为元，精确到小数点后两位
	Acctname  string `json:"acctname"`   // 付款银行户名，付款方式为银行卡支付和企业网银B2B支付方式时返回
	Acctno    string `json:"acctno"`     // 付款银行账号，付款方式为银行卡支付和企业网银B2B支付方式时返回，脱敏返回：卡号前六后四
}

type QueryPaymentPayeeInfo struct {
	PayeeType string `json:"payee_type"` // 收款方类型。用户：USER平台商户：MERCHANT
	PayeeID   string `json:"payee_id"`   // 收款方标识。用户的user_id或者平台商户商户号
	Amount    string `json:"amount"`     // 付款金额。付款方式对应的金额，单位为元，精确到小数点后两位
}

// 退款申请
type MorePayeeRefundResponse struct {
	RetCode     string `json:"ret_code"`     // 请求结果代码
	RetMsg      string `json:"ret_msg"`      // 请求结果描述
	OidPartner  string `json:"oid_partner"`  // ACCP系统分配给平台商户的唯一编号
	UserID      string `json:"user_id"`      // 用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。由商户自定义
	TxnSeqno    string `json:"txn_seqno"`    // 商户系统唯一交易流水号。
	TotalAmount string `json:"total_amount"` // 订单总金额，单位为元，精确到小数点后两位
	AccpTxno    string `json:"accp_txno"`    // ACCP系统交易单号
}

// 退款结果查询
type QueryRefundResponse struct {
	RetCode        string                     `json:"ret_code"`        // 请求结果代码
	RetMsg         string                     `json:"ret_msg"`         // 请求结果描述
	OidPartner     string                     `json:"oid_partner"`     // ACCP系统分配给平台商户的唯一编号
	AccountingDate string                     `json:"accounting_date"` // 账务日期。ACCP系统交易账务日期，交易成功时返回，格式：yyyyMMdd
	AccpTxno       string                     `json:"accp_txno"`       // ACCP系统退款单号
	RefundAmount   string                     `json:"refund_amount"`   // 已退金额，单位为元，精确到小数点后两位。
	TxnStatus      string                     `json:"txn_status"`      // 退款交易状态。TRADE_SUCCESS：退款成功 TRADE_FAILURE：退款失败 TRADE_PROCESSING：退款处理中。退款结果以此为准，商户按此进行后续业务逻辑处理
	RefundMethods  []QueryRefundRefundMethods `json:"refundMethods"`   // 原付款方式退款规则信息
}

type QueryRefundRefundMethods struct {
	RefundAmount string `json:"refund_amount"` // 退款金额，付款方式对应的退款金额，单位为元，精确到小数点后两位 所有的付款方式退款金额相加必须和退款总金额一致
	Method       string `json:"method"`        // 付款方式 https://open.lianlianpay.com/docs/accp/accpstandard/accp-async-notification-payment.html
}

// 退款结果异步通知
type MorePayeeRefundNotifyRequest struct {
	OidPartner     string `json:"oid_partner"`     // 商户号，ACCP系统分配给平台商户的唯一编号
	AccountingDate string `json:"accounting_date"` // 账务日期，ACCP系统交易账务日期，交易成功时返回，格式为yyyyMMdd
	AccpTxno       string `json:"accp_txno"`       // ACCP系统交易单号。用于标识一次通知；同一个消息的重复通知该字段相同
	RefundSeqno    string `json:"refund_seqno"`    // 退款订单号。标识一次退款请求，商户系统需要保证唯一
	RefundAmount   string `json:"refund_amount"`   // 退款金额，单位为元，精确到小数点后两位
	TxnStatus      string `json:"txn_status"`      // 退款交易状态。TRADE_SUCCESS：退款成功 TRADE_FAILURE：退款失败 退款结果以此为准，商户按此进行后续业务逻辑处理
	OrderInfo      string `json:"order_info"`      // 订单信息，原样返回创单时传的订单信息
	RefundMethods  string `json:"refundMethods"`   // 原付款方式退款规则信息
}

// 提现申请
type WithdrawalResponse struct {
	RetCode         string `json:"ret_code"`         // 请求结果代码
	RetMsg          string `json:"ret_msg"`          // 请求结果描述
	OidPartner      string `json:"oid_partner"`      // ACCP系统分配给平台商户的唯一编号
	UserID          string `json:"user_id"`          // 用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。由商户自定义
	TxnSeqno        string `json:"txn_seqno"`        // 商户系统唯一交易流水号。由商户自定义
	TotalAmount     string `json:"total_amount"`     // 订单总金额，单位为元，精确到小数点后两位
	AccpTxno        string `json:"accp_txno"`        // ACCP系统交易单号
	FeeAmountNumber string `json:"fee_amountNumber"` // 手续费金额，单位为元，精确到小数点后两位。会自动收取到平台商户的自有资金账户。不允许超过订单总金额的20%
	Token           string `json:"token"`            // 支付授权令牌，有效期30分钟。当交易需要二次验证的时候，需要通过token调用交易二次短信验证接口
}

// 提现结果异步通知
type WithdrawalNotifyRequest struct {
	OidPartner     string                    `json:"oid_partner"`     // 商户号，ACCP系统分配给平台商户的唯一编号
	UserID         string                    `json:"user_id"`         // 用户在商户系统中的唯一编号
	AccountingDate string                    `json:"accounting_date"` // 账务日期，ACCP系统交易账务日期，交易成功时返回，格式为yyyyMMdd
	FinishTime     string                    `json:"finish_time"`     // 提现成功时间。格式：yyyyMMddHHmmss
	AccpTxno       int64                     `json:"accp_txno"`       // ACCP系统交易单号
	TxnStatus      string                    `json:"txn_status"`      // 提现交易状态。RADE_SUCCESS:交易成功 TRADE_FAILURE:交易失败 TRADE_CANCEL：退汇。提现最终结果以此为准，商户按该字段值进行后续业务逻辑处理
	Bankcode       string                    `json:"bankcode"`        // 银行编码。提现收款银行编码
	OrderInfo      WithdrawalNotifyOrderInfo `json:"orderInfo"`       // 商户订单信息
	PayerInfo      WithdrawalNotifyPayerInfo `json:"payerInfo"`
}

type WithdrawalNotifyOrderInfo struct {
	TxnSeqno    string `json:"txn_seqno"`    // 商户系统唯一交易流水号。由商户自定义
	TxnTime     string `json:"txn_time"`     // 商户系统交易时间。格式：yyyyMMddHHmmss
	TotalAmount string `json:"total_amount"` // 提现总金额，单位为元，精确到小数点后两位
	OrderInfo   string `json:"order_info"`   // 订单信息。用于订单说明，透传返回
}

type WithdrawalNotifyPayerInfo struct {
	PayerType string `json:"payer_type"` // 付款方类型。 用户：USER 平台商户：MERCHANT
	PayerID   string `json:"payer_id"`   // 付款方标识。付款方为用户时设置user_id 。付款方为商户时设置平台商户号
}

// 提现结果查询
type QueryWithdrawalResponse struct {
	RetCode        string                   `json:"ret_code"`        // 请求结果代码
	RetMsg         string                   `json:"ret_msg"`         // 请求结果描述
	OidPartner     string                   `json:"oid_partner"`     // ACCP系统分配给平台商户的唯一编号
	AccountingDate string                   `json:"accounting_date"` // 账务日期。ACCP系统交易账务日期，交易成功时返回，格式：yyyyMMdd
	FinishTime     string                   `json:"finish_time"`     // 支付完成时间,格式为yyyyMMddHHmmss
	AccpTxno       string                   `json:"accp_txno"`       // ACCP系统交易单号
	TxnStatus      string                   `json:"txn_status"`      // TRADE_SUCCESS：交易成功 TRADE_FAILURE：交易失败 TRADE_CANCEL：退汇TRADE_PREPAID：预付完成
	Bankcode       string                   `json:"bankcode"`        // 银行编码，提现收款银行编码
	OrderInfo      QueryWithdrawalOrderInfo `json:"orderInfo"`       // 商户订单信息
	PayerInfo      QueryWithdrawalPayerInfo `json:"payerInfo"`       // 付款方信息
}

type QueryWithdrawalOrderInfo struct {
	TxnSeqno    string `json:"txn_seqno"`    // 商户系统唯一交易流水号
	TxnTime     string `json:"txn_time"`     // 商户系统交易时间。格式：yyyyMMddHHmmss
	TotalAmount string `json:"total_amount"` // 订单总金额，单位为元，精确到小数点后两位
	OrderInfo   string `json:"order_info"`   // 用于订单说明，透传返回
}

type QueryWithdrawalPayerInfo struct {
	PayerType string `json:"payer_type"` // 付款方类型。 用户：USER 平台商户：MERCHANT
	PayerID   string `json:"payer_id"`   // 付款方标识。付款方为用户时设置user_id付款方为商户时设置平台商户号
}

// 内部代发申请
type TransferMorepyeeResponse struct {
	RetCode        string `json:"ret_code"`        // 请求结果代码
	RetMsg         string `json:"ret_msg"`         // 请求结果描述
	OidPartner     string `json:"oid_partner"`     // ACCP系统分配给平台商户的唯一编号
	UserID         string `json:"user_id"`         // 用户在商户系统中的唯一编号，要求该编号在商户系统能唯一标识用户。由商户自定义
	TxnSeqno       string `json:"txn_seqno"`       // 商户系统唯一交易流水号。由商户自定义
	TotalAmount    string `json:"total_amount"`    // 订单总金额，单位为元，精确到小数点后两位
	AccpTxno       string `json:"accp_txno"`       // ACCP系统交易单号
	AccountingDate string `json:"accounting_date"` // 账务日期。ACCP系统交易账务日期，交易成功时返回，格式：yyyyMMdd
}
