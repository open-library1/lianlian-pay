<!--
 * @Descripttion: 
 * @version: 
 * @Author: wanglei
 * @Date: 2023-04-26 12:06:24
 * @LastEditors: wanglei
 * @LastEditTime: 2023-04-27 10:12:03
-->
##支付SDK配置使用


### 获取配置实例化配置信息

```
package config

import (
	"io/ioutil"
	"os"

	"github.com/go-pay/gopay/pkg/xlog"
	lianlianPay "gitlab.com/open-library1/lianlian-pay"
)

var LianlianPayClient *lianlianPay.Client

// LianLianInit 初始化连连信息
func LianLianInit() {
	// 商户号
	oidPartner := "2020042200284052"
	publicKeyPath := "merchant_rsa_public_key.pem"
	privateKeyPath := "merchant_rsa_private_key.pem"
	publicKey, err := ioutil.ReadFile(publicKeyPath)
	if err != nil {
		xlog.Color(xlog.Red).Error("连连支付公钥地址不存在:", publicKeyPath)
		os.Exit(29)
	}
	privateKey, err := ioutil.ReadFile(privateKeyPath)
	if err != nil {
		xlog.Color(xlog.Red).Error("连连支付私钥地址不存在:", privateKeyPath)
		os.Exit(33)
	}
	LianlianPayClient = lianlianPay.NewClient(oidPartner, privateKey, publicKey, false, "on")
	// 区分环境 测试/生产
	LianlianPayClient.SetCountry()
	// 签名算法 RSA/HMAC-SHA256
	LianlianPayClient.SignType = "RSA"
}

```

### 业务调取，参数组装

```
//支付调取
func callLianLianPay(goodsName, tradeNum, expire string, userId int, payMoney float64) (map[string]string, error) {
	result := make(map[string]string)
	timeStamp := utils.TimestampToDatetimeNoStr(time.Now().Unix())
	//初始化参数Map
	bm := make(pay_lianlian.BodyMap)
	bm.Set("api_version", "1.0").
		Set("time_stamp", timeStamp).
		Set("platform", "").
		Set("user_id", userId).
		Set("busi_partner", pay_lianlian.BusiPartnerVirtual).
		Set("no_order", tradeNum).
		Set("dt_order", timeStamp).
		Set("name_goods", goodsName).
		Set("money_order", payMoney).
		Set("notify_url", config.Conf.Yaml.PayLianLian.Notifyurl).
		Set("risk_item", fmt.Sprintf("{\"user_info_bind_phone\":\"19933610000\",\"user_info_dt_register\":\"%s\",\"frms_ware_category \":\"1009\"}", timeStamp)).
		Set("flag_pay_product", "0").
		Set("flag_chnl", pay_lianlian.FlagChnlH5).
		Set("bank_code", "0100000")
	//Set("card_type", "")
	_, err := config.Conf.LianLianPayClient.UnifiedOrder(context.Background(), bm)
	if err != nil {
		return result, err
	}
	return result, nil
}

```

### RSAWithMd5 签名方式

+参考文档
https://github.com/wenzhenxi/gorsa/blob/15feec0f05a6feb896e30fbf3c9df5d77fa1a421/gorsaSign.go