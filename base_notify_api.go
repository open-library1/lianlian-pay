/*
 * @Descripttion:
 * @version:
 * @Author: wanglei
 * @Date: 2023-04-26 12:06:24
 * @LastEditors: wanglei
 * @LastEditTime: 2023-05-06 18:22:10
 */
package pay_lianlian

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"

	"gitlab.com/open-library1/lianlian-pay/xhttp"
	"gitlab.com/open-library1/lianlian-pay/xlog"
)

// 提现结果异步通知
// https://open.lianlianpay.com/docs/accp/accpstandard/accp-async-notification-withdrawal.html
func (w *Client) WithdrawalNotify(requestBody io.ReadCloser, requestHeader http.Header) (orderRsp *WithdrawalNotifyRequest, err error) {
	data, err := ioutil.ReadAll(requestBody)
	if err != nil {
		return
	}

	// 增加日志
	headerByte, _ := json.Marshal(requestHeader)
	xlog.Color(xlog.Red).Error("提现结果异步通知=======Request:", string(data), "RequestHeader:", string(headerByte))

	// 获取签名数据
	sign, signType, err := GetHeaderSignInfo(requestHeader)
	if err != nil {
		return nil, err
	}
	// 验证响应签名
	ok := VerifySign(w.LlPayPublicKey, signType, string(data), sign)
	if !ok {
		return nil, errors.New("Lianlian_err: 验证签名错误")
	}

	// 获取异步通知信息
	orderRsp = new(WithdrawalNotifyRequest)
	if err = json.Unmarshal(data, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(data))
	}

	return orderRsp, nil
}

// 退款结果异步通知
// https://open.lianlianpay.com/docs/accp/accpstandard/accp-async-notification-refund.html
func (w *Client) MorePayeeRefundNotify(requestBody io.ReadCloser, requestHeader http.Header) (orderRsp *MorePayeeRefundNotifyRequest, err error) {
	data, err := ioutil.ReadAll(requestBody)
	if err != nil {
		return
	}

	// 增加日志
	headerByte, _ := json.Marshal(requestHeader)
	xlog.Color(xlog.Red).Error("退款结果异步通知=======Request:", string(data), "RequestHeader:", string(headerByte))

	// 获取签名数据
	sign, signType, err := GetHeaderSignInfo(requestHeader)
	if err != nil {
		return nil, err
	}
	// 验证响应签名
	ok := VerifySign(w.LlPayPublicKey, signType, string(data), sign)
	if !ok {
		return nil, errors.New("Lianlian_err: 验证签名错误")
	}

	// 获取异步通知信息
	orderRsp = new(MorePayeeRefundNotifyRequest)
	if err = json.Unmarshal(data, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(data))
	}

	return orderRsp, nil
}

// 支付结果异步通知
// https://open.lianlianpay.com/docs/accp/accpstandard/accp-async-notification-payment.html
func (w *Client) PaymentNotify(requestBody io.ReadCloser, requestHeader http.Header) (orderRsp *PaymentNotifyRequest, err error) {
	data, err := ioutil.ReadAll(requestBody)
	if err != nil {
		return
	}

	// 增加日志
	headerByte, _ := json.Marshal(requestHeader)
	xlog.Color(xlog.Red).Error("充值/消费支付结果异步通知=======Request:", string(data), "RequestHeader:", string(headerByte))

	// 获取签名数据
	sign, signType, err := GetHeaderSignInfo(requestHeader)
	if err != nil {
		return nil, err
	}
	// 验证响应签名
	ok := VerifySign(w.LlPayPublicKey, signType, string(data), sign)
	if !ok {
		return nil, errors.New("Lianlian_err: 验证签名错误")
	}

	// 获取异步通知信息
	orderRsp = new(PaymentNotifyRequest)
	if err = json.Unmarshal(data, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(data))
	}

	return orderRsp, nil
}

// 银行卡解绑结果异步通知
// https://open.lianlianpay.com/docs/accp/accpstandard/accp-async-notification-unlinkedacct-ind-apply.html
func (w *Client) UnlinkedacctIndApplyNotify(requestBody io.ReadCloser, requestHeader http.Header) (orderRsp *UnlinkedacctIndApplyNotifyRequest, err error) {
	data, err := ioutil.ReadAll(requestBody)
	if err != nil {
		return
	}

	// 增加日志
	headerByte, _ := json.Marshal(requestHeader)
	xlog.Color(xlog.Red).Error("银行卡解绑结果异步通知=======Request:", string(data), "RequestHeader:", string(headerByte))

	// 获取签名数据
	sign, signType, err := GetHeaderSignInfo(requestHeader)
	if err != nil {
		return nil, err
	}
	// 验证响应签名
	ok := VerifySign(w.LlPayPublicKey, signType, string(data), sign)
	if !ok {
		return nil, errors.New("Lianlian_err: 验证签名错误")
	}

	// 获取异步通知信息
	orderRsp = new(UnlinkedacctIndApplyNotifyRequest)
	if err = json.Unmarshal(data, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(data))
	}

	return orderRsp, nil
}

// 个人新增绑卡异步通知
// https://open.lianlianpay.com/docs/accp/accpstandard/accp-async-notification-individual-bindcard.html
func (w *Client) IndividualBindcardApplyNotify(requestBody io.ReadCloser, requestHeader http.Header) (orderRsp *IndividualBindcardApplyNotifyRequest, err error) {
	data, err := ioutil.ReadAll(requestBody)
	if err != nil {
		return
	}

	// 增加日志
	headerByte, _ := json.Marshal(requestHeader)
	xlog.Color(xlog.Red).Error("个人新增绑卡异步通知=======Request:", string(data), "RequestHeader:", string(headerByte))

	// 获取签名数据
	sign, signType, err := GetHeaderSignInfo(requestHeader)
	if err != nil {
		return nil, err
	}
	// 验证响应签名
	ok := VerifySign(w.LlPayPublicKey, signType, string(data), sign)
	if !ok {
		return nil, errors.New("Lianlian_err: 验证签名错误")
	}

	// 获取异步通知信息
	orderRsp = new(IndividualBindcardApplyNotifyRequest)
	if err = json.Unmarshal(data, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(data))
	}

	return orderRsp, nil
}

// 用户开户异步通知 【页面接入】
// https://open.lianlianpay.com/docs/accp/accpstandard/accp-async-notification-openacct-apply.html
func (w *Client) OpenacctApplyNotify(requestBody io.ReadCloser, requestHeader http.Header) (orderRsp *OpenacctApplyNotifyRequest, err error) {
	data, err := ioutil.ReadAll(requestBody)
	if err != nil {
		return
	}

	// 增加日志
	headerByte, _ := json.Marshal(requestHeader)
	xlog.Color(xlog.Red).Error("个人开户异步通知=======Request:", string(data), "RequestHeader:", string(headerByte))

	// 获取签名数据
	sign, signType, err := GetHeaderSignInfo(requestHeader)
	if err != nil {
		return nil, err
	}
	// 验证响应签名
	ok := VerifySign(w.LlPayPublicKey, signType, string(data), sign)
	if !ok {
		return nil, errors.New("Lianlian_err: 验证签名错误")
	}

	// 获取异步通知信息
	orderRsp = new(OpenacctApplyNotifyRequest)
	if err = json.Unmarshal(data, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(data))
	}

	return orderRsp, nil
}

// 个人销户异步通知
// https://open.lianlianpay.com/docs/accp/accpstandard/accp-async-notification-cancel.html
func (w *Client) CancelApplyNotify(requestBody io.ReadCloser, requestHeader http.Header) (orderRsp *CancelApplyNotifyRequest, err error) {
	data, err := ioutil.ReadAll(requestBody)
	if err != nil {
		return
	}

	// 增加日志
	headerByte, _ := json.Marshal(requestHeader)
	xlog.Color(xlog.Red).Error("个人销户异步通知=======Request:", string(data), "RequestHeader:", string(headerByte))

	// 获取签名数据
	sign, signType, err := GetHeaderSignInfo(requestHeader)
	if err != nil {
		return nil, err
	}
	// 验证响应签名
	ok := VerifySign(w.LlPayPublicKey, signType, string(data), sign)
	if !ok {
		return nil, errors.New("Lianlian_err: 验证签名错误")
	}

	// 获取异步通知信息
	orderRsp = new(CancelApplyNotifyRequest)
	if err = json.Unmarshal(data, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(data))
	}

	return orderRsp, nil
}

// 个人开户异步通知
// https://open.lianlianpay.com/docs/accp/accpstandard/accp-async-notification-openacct-individual.html
func (w *Client) OpenAccountNotify(requestBody io.ReadCloser, requestHeader http.Header) (orderRsp *OpenAccountNotifyRequest, err error) {
	data, err := ioutil.ReadAll(requestBody)
	if err != nil {
		return
	}

	// 获取签名数据
	sign, signType, err := GetHeaderSignInfo(requestHeader)
	if err != nil {
		return nil, err
	}
	// 验证响应签名
	ok := VerifySign(w.LlPayPublicKey, signType, string(data), sign)
	if !ok {
		return nil, errors.New("Lianlian_err: 验证签名错误")
	}

	// 获取异步通知信息
	orderRsp = new(OpenAccountNotifyRequest)
	if err = json.Unmarshal(data, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(data))
	}

	return orderRsp, nil
}
