/*
 * @Descripttion:
 * @version:
 * @Author: wanglei
 * @Date: 2023-04-26 12:06:24
 * @LastEditors: wanglei
 * @LastEditTime: 2023-04-27 18:18:44
 */
package pay_lianlian

import (
	"crypto"
	"crypto/hmac"
	"crypto/md5"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/hex"
	"encoding/pem"
	"errors"
	"hash"
	"net/http"

	"gitlab.com/open-library1/lianlian-pay/xlog"
)

//签名方式
func GetReleaseSign(privateKey []byte, signType string, bm BodyMap) (sign string) {
	// 获取请求参数
	sortStr := BodyMapSotrJson(bm)

	// 获取秘钥信息
	block, _ := pem.Decode(privateKey)
	if block == nil {
		xlog.Error("GetReleaseSign privateKey empty: %s", "")
		return ""
	}
	priv, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		xlog.Error("ParsePKCS1PrivateKey  error: %s", err.Error())
		return ""
	}

	// 区分加密方式
	var h hash.Hash
	var resEnc []byte
	if signType == SignType_HMAC_SHA256 {
		h = hmac.New(sha256.New, privateKey)
		h.Write([]byte(sortStr))
		hashMd5 := h.Sum([]byte(nil))
		resEnc, err = rsa.SignPKCS1v15(nil, priv, crypto.SHA256, hashMd5[:])
	} else {
		h = md5.New()
		h.Write([]byte(sortStr))
		hashMd5 := h.Sum([]byte(nil))
		hashMdParams := md5.Sum([]byte(hex.EncodeToString(hashMd5)))
		resEnc, err = rsa.SignPKCS1v15(nil, priv, crypto.MD5, hashMdParams[:])
	}

	if err != nil {
		xlog.Error("SignPKCS1v15  error: %s", err.Error())
		return ""
	}
	return base64.StdEncoding.EncodeToString(resEnc)
	//return hex.EncodeToString(resEnc)
}

// 获取签名信息
func GetHeaderSignInfo(Header http.Header) (sign, signType string, err error) {
	// 获取签名数据
	resSignatureData, ok := Header["Signature-Data"]
	if !ok {
		return sign, signType, errors.New("Lianlian_err: 响应签名错误")
	}
	// 获取签名算法
	resSignatureType, ok := Header["Signature-Type"]
	if !ok {
		return sign, signType, errors.New("Lianlian_err: 响应签名算法类型错误")
	}

	return resSignatureData[0], resSignatureType[0], nil
}

// 验证签名
func VerifySign(llPayPublicKey []byte, signType string, content, sign string) (ok bool) {
	block, _ := pem.Decode(llPayPublicKey)
	if block == nil {
		return false
	}
	pub, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return false
	}

	// 转义签名
	signData, err := base64.StdEncoding.DecodeString(sign)
	if err != nil {
		return false
	}

	// 区分算法
	var h hash.Hash
	if signType == SignType_HMAC_SHA256 {
		h = hmac.New(sha256.New, llPayPublicKey)
		h.Write([]byte(content))
		hashMd5 := h.Sum([]byte(nil))
		// 验证签名
		err = rsa.VerifyPKCS1v15(pub.(*rsa.PublicKey), crypto.MD5, hashMd5[:], signData)
		if err == nil {
			ok = true
		}

	} else {
		h = md5.New()
		h.Write([]byte(content))
		hashMd5 := h.Sum([]byte(nil))
		hashMdParams := md5.Sum([]byte(hex.EncodeToString(hashMd5)))
		// 验证签名
		err = rsa.VerifyPKCS1v15(pub.(*rsa.PublicKey), crypto.MD5, hashMdParams[:], signData)
		if err == nil {
			ok = true
		}
	}

	return
}
