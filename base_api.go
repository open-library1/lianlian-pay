/*
 * @Descripttion:
 * @version:
 * @Author: wanglei
 * @Date: 2023-04-26 12:06:24
 * @LastEditors: wanglei
 * @LastEditTime: 2023-05-09 16:39:15
 */
package pay_lianlian

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	"gitlab.com/open-library1/lianlian-pay/xhttp"
)

// 收款-统一下单
// 文档地址：https://open.lianlianpay.com/apis/unified-payment.html

func (w *Client) UnifiedOrder(ctx context.Context, bm BodyMap) (orderRsp *UnifiedOrderResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "txn_type", "orderInfo")
	if err != nil {
		return nil, err
	}
	var bs []byte
	if w.IsProd {
		bs, err = w.doProdPost(ctx, bm, w.BaseDomain+tradeCreate, nil)
	} else {
		bm.Set("total_amount", 0.01)
		bs, err = w.doProdPost(ctx, bm, w.BaseDomain+tradeCreate, nil)
	}
	if err != nil {
		return nil, err
	}
	orderRsp = new(UnifiedOrderResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 绑定手机号验证申请
// https://open.lianlianpay.com/docs/accp/accpstandard/regphone-verifycode-apply.html
func (w *Client) RegphoneVerifycodeApply(ctx context.Context, bm BodyMap) (orderRsp *RegphoneVerifycodeApplyResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "user_id", "reg_phone")
	if err != nil {
		return nil, err
	}
	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+regphoneVerifycodeApply, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(RegphoneVerifycodeApplyResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 文件上传
// https://open.lianlianpay.com/docs/accp/accpstandard/upload.html
func (w *Client) FileUpload(ctx context.Context, bm BodyMap) (orderRsp *FileUploadResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "user_id", "txn_seqno", "txn_time", "file_type", "context_type", "file_context")
	if err != nil {
		return nil, err
	}
	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseFileDomain+fileUpload, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(FileUploadResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 绑定手机验证码验证
// https://open.lianlianpay.com/docs/accp/accpstandard/regphone-verifycode-verify.html
func (w *Client) RegphoneVerifyCodeVerify(ctx context.Context, bm BodyMap) (orderRsp *RegphoneVerifyCodeVerifyResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "user_id", "reg_phone", "verify_code")
	if err != nil {
		return nil, err
	}
	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+regphoneVerifycodeVerify, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(RegphoneVerifyCodeVerifyResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 个人用户开户申请
// https://open.lianlianpay.com/docs/accp/accpstandard/openacct-apply-individual.html
func (w *Client) OpenacctApplyIndividual(ctx context.Context, bm BodyMap) (orderRsp *OpenacctApplyIndividualResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "user_id", "txn_seqno", "txn_time", "notify_url", "basicInfo")
	if err != nil {
		return nil, err
	}
	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+openacctApplyIndividual, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(OpenacctApplyIndividualResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 个人用户开户验证
// https://open.lianlianpay.com/docs/accp/accpstandard/openacct-verify-individual.html
func (w *Client) OpenacctVerifyIndividual(ctx context.Context, bm BodyMap) (orderRsp *OpenacctVerifyIndividualResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "user_id", "txn_seqno", "token", "password", "random_key")
	if err != nil {
		return nil, err
	}
	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+openacctVerifyIndividual, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(OpenacctVerifyIndividualResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 随机密码因子获取
// https://open.lianlianpay.com/docs/accp/accpstandard/get-random.html
func (w *Client) GetRandoml(ctx context.Context, bm BodyMap) (orderRsp *GetRandomlResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "user_id", "txn_seqno", "token", "password", "random_key")
	if err != nil {
		return nil, err
	}
	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+getRandoml, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(GetRandomlResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 申请密码控件Token
// https://open.lianlianpay.com/docs/accp/accpstandard/apply-password-element.html
func (w *Client) ApplyPasswordElement(ctx context.Context, bm BodyMap) (orderRsp *ApplyPasswordElementResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "password_scene", "flag_chnl")
	if err != nil {
		return nil, err
	}
	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BasePwdDomain+applyPasswordElement, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(ApplyPasswordElementResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 用户开户【页面接入】
// https://open.lianlianpay.com/docs/accp/accpstandard/openacct-apply.html
func (w *Client) OpenacctApply(ctx context.Context, bm BodyMap) (orderRsp *OpenacctApplyResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "user_id", "txn_seqno", "txn_time", "notify_url", "basicInfo")
	if err != nil {
		return nil, err
	}
	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseOpenAccountDomain+openacctApply, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(OpenacctApplyResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 销户申请
// https://open.lianlianpay.com/docs/accp/accpstandard/cancel-apply.html
func (w *Client) CancelApply(ctx context.Context, bm BodyMap) (orderRsp *CancelApplyResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "user_id", "txn_seqno", "txn_time", "notify_url", "user_name", "id_type", "id_no", "reg_phone", "password", "random_key")
	if err != nil {
		return nil, err
	}
	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+cancelApply, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(CancelApplyResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 用户信息查询
// https://open.lianlianpay.com/docs/accp/accpstandard/query-userinfo.html
func (w *Client) QueryUserinfo(ctx context.Context, bm BodyMap) (orderRsp *QueryUserinfoResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "user_id")
	if err != nil {
		return nil, err
	}
	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+queryUserinfo, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(QueryUserinfoResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 账户信息查询
// https://open.lianlianpay.com/docs/accp/accpstandard/query-acctinfo.html
func (w *Client) QueryAcctinfo(ctx context.Context, bm BodyMap) (orderRsp *QueryAcctinfoResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "user_type")
	if err != nil {
		return nil, err
	}
	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+queryAcctinfo, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(QueryAcctinfoResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 资金流水列表查询
// https://open.lianlianpay.com/docs/accp/accpstandard/query-acctserial.html
func (w *Client) QueryAcctserial(ctx context.Context, bm BodyMap) (orderRsp *QueryAcctserialResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "user_id", "user_type", "acct_type", "date_start", "date_end", "page_no", "page_size")
	if err != nil {
		return nil, err
	}
	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+queryAcctserial, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(QueryAcctserialResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 资金流水详情查询
// https://open.lianlianpay.com/docs/accp/accpstandard/query-acctserialdetail.html
func (w *Client) QueryAcctserialdetail(ctx context.Context, bm BodyMap) (orderRsp *QueryAcctserialdetailResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "user_id", "user_type", "jno_acct")
	if err != nil {
		return nil, err
	}
	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+queryAcctserialdetail, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(QueryAcctserialdetailResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 个人用户新增绑卡申请
// https://open.lianlianpay.com/docs/accp/accpstandard/individual-bindcard-apply.html
func (w *Client) IndividualBindcardApply(ctx context.Context, bm BodyMap) (orderRsp *IndividualBindcardApplyResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "user_id", "txn_seqno", "txn_time", "notify_url", "linked_acctno", "linked_phone", "password", "random_key")
	if err != nil {
		return nil, err
	}
	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+individualBindcardApply, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(IndividualBindcardApplyResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 个人用户新增绑卡验证
// https://open.lianlianpay.com/docs/accp/accpstandard/individual-bindcard-verify.html
func (w *Client) IndividualBindcardVerify(ctx context.Context, bm BodyMap) (orderRsp *IndividualBindcardVerifyResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "user_id", "txn_seqno", "token", "verify_code")
	if err != nil {
		return nil, err
	}
	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+individualBindcardVerify, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(IndividualBindcardVerifyResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 个人用户解绑银行卡
// https://open.lianlianpay.com/docs/accp/accpstandard/unlinkedacct-ind-apply.html
func (w *Client) UnlinkedacctIndApply(ctx context.Context, bm BodyMap) (orderRsp *UnlinkedacctIndApplyResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "user_id", "txn_seqno", "txn_time", "notify_url", "linked_acctno", "password", "random_key")
	if err != nil {
		return nil, err
	}
	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+unlinkedacctIndApply, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(UnlinkedacctIndApplyResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 绑卡信息查询
// https://open.lianlianpay.com/docs/accp/accpstandard/query-linkedacct.html
func (w *Client) QueryLinkedacct(ctx context.Context, bm BodyMap) (orderRsp *QueryLinkedacctResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "user_id")
	if err != nil {
		return nil, err
	}

	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+queryLinkedacct, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(QueryLinkedacctResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 银行卡快捷支付
// https://open.lianlianpay.com/docs/accp/accpstandard/payment-bankcard.html
func (w *Client) PaymentBankcard(ctx context.Context, bm BodyMap) (orderRsp *PaymentBankcardResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "txn_seqno", "total_amount", "risk_item", "payerInfo")
	if err != nil {
		return nil, err
	}

	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+paymentBankcard, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(PaymentBankcardResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 余额支付
// https://open.lianlianpay.com/docs/accp/accpstandard/payment-balance.html
func (w *Client) PaymentBalance(ctx context.Context, bm BodyMap) (orderRsp *PaymentBalanceResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "txn_seqno", "total_amount", "risk_item", "payerInfo")
	if err != nil {
		return nil, err
	}

	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+paymentBalance, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(PaymentBalanceResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 交易二次短信验证
// https://open.lianlianpay.com/docs/accp/accpstandard/validation-sms.html
func (w *Client) ValidationSms(ctx context.Context, bm BodyMap) (orderRsp *ValidationSmsResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "payer_type", "payer_id", "txn_seqno", "total_amount", "token", "verify_code")
	if err != nil {
		return nil, err
	}

	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+validationSms, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(ValidationSmsResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 账户+收银台
// https://open.lianlianpay.com/docs/accp/accpstandard/accp-cashier-paycreate.html
func (w *Client) Paycreate(ctx context.Context, bm BodyMap) (orderRsp *PaycreateResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "txn_type", "flag_chnl", "risk_item", "orderInfo", "payerInfo")
	if err != nil {
		return nil, err
	}

	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseOpenAccountDomain+paycreate, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(PaycreateResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 交易结果查询 消费/充值/内部代发交易查询
// https://open.lianlianpay.com/docs/accp/accpstandard/query-payment.html
func (w *Client) QueryPayment(ctx context.Context, bm BodyMap) (orderRsp *QueryPaymentResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner")
	if err != nil {
		return nil, err
	}

	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+queryPayment, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(QueryPaymentResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 退款申请
// https://open.lianlianpay.com/docs/accp/accpstandard/more-payee-refund.html
func (w *Client) MorePayeeRefund(ctx context.Context, bm BodyMap) (orderRsp *MorePayeeRefundResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "user_id", "originalOrderInfo", "refundOrderInfo", "pyeeRefundInfos", "refundMethods")
	if err != nil {
		return nil, err
	}

	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+morePayeeRefund, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(MorePayeeRefundResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 退款结果查询
// https://open.lianlianpay.com/docs/accp/accpstandard/query-refund.html
func (w *Client) QueryRefund(ctx context.Context, bm BodyMap) (orderRsp *QueryRefundResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner")
	if err != nil {
		return nil, err
	}

	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+queryRefund, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(QueryRefundResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	if orderRsp.RetCode != RET_CODE_SUCCESS {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 提现申请
// https://open.lianlianpay.com/docs/accp/accpstandard/withdrawal.html
func (w *Client) Withdrawal(ctx context.Context, bm BodyMap) (orderRsp *WithdrawalResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "risk_item", "orderInfo", "payerInfo")
	if err != nil {
		return nil, err
	}

	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+withdrawal, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(WithdrawalResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	// 请求结果代码。0000 表示提现申请成功，最终提现处理结果以异步通知为 8889 表示提现申请待确认成功，需要调用提现确认完成提现创单申请 8888 表示提现需要再次信息短信验证码校验
	if orderRsp.RetCode != RET_CODE_SUCCESS && orderRsp.RetCode != "8888" && orderRsp.RetCode != "8889" {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 提现结果查询
// https://open.lianlianpay.com/docs/accp/accpstandard/query-withdrawal.html
func (w *Client) QueryWithdrawal(ctx context.Context, bm BodyMap) (orderRsp *QueryWithdrawalResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner")
	if err != nil {
		return nil, err
	}

	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+queryWithdrawal, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(QueryWithdrawalResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	// 请求结果代码。0000 表示提现申请成功，最终提现处理结果以异步通知为 8889 表示提现申请待确认成功，需要调用提现确认完成提现创单申请 8888 表示提现需要再次信息短信验证码校验
	if orderRsp.RetCode != RET_CODE_SUCCESS && orderRsp.RetCode != "8888" && orderRsp.RetCode != "8889" {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}

// 内部代发申请
// https://open.lianlianpay.com/docs/accp/accpstandard/transfer-morepyee.html
func (w *Client) TransferMorepyee(ctx context.Context, bm BodyMap) (orderRsp *TransferMorepyeeResponse, err error) {
	err = bm.CheckEmptyError("timestamp", "oid_partner", "risk_item", "orderInfo", "payerInfo", "payeeInfo")
	if err != nil {
		return nil, err
	}

	var bs []byte
	bs, err = w.doProdPost(ctx, bm, w.BaseDomain+transferMorepyee, nil)
	if err != nil {
		return nil, err
	}
	orderRsp = new(TransferMorepyeeResponse)
	if err = json.Unmarshal(bs, orderRsp); err != nil {
		return nil, fmt.Errorf("[%w]: %v, bytes: %s", xhttp.UnmarshalErr, err, string(bs))
	}
	// 请求结果代码。0000 表示提现申请成功，最终提现处理结果以异步通知为 8889 表示提现申请待确认成功，需要调用提现确认完成提现创单申请 8888 表示提现需要再次信息短信验证码校验
	if orderRsp.RetCode != RET_CODE_SUCCESS && orderRsp.RetCode != "8888" && orderRsp.RetCode != "8889" {
		return nil, errors.New(orderRsp.RetMsg)
	}
	return orderRsp, nil
}
